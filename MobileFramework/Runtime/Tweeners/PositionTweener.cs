using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using DG.Tweening;

public class PositionTweener : MonoBehaviour
{
    [Header("Events")]
    [SerializeField] private UnityEvent onEndAnimation;
    [SerializeField] private UnityEvent onEndAnimationReverse;

    [Header("Base Variables")]
    [SerializeField] private Vector3 endValue;
    [SerializeField] private float duration;
    [SerializeField] private bool snapping;
    [SerializeField] private bool isLocal;

    [Header("Ease")]
    [SerializeField] private AnimationCurve ease;

    [Header("Loop")]
    [SerializeField] private int loopCount;
    [SerializeField] private LoopType type;

    private Vector3 startPosition;

    private void Start()
    {
        if (isLocal)
        {
            startPosition = transform.localPosition;
        }
        else
        {
            startPosition = transform.position;
        }
    }

    public void Animate()
    {
        if (isLocal)
        {
            transform.DOLocalMove(endValue, duration, snapping).SetLoops(loopCount, type).SetEase(ease).OnComplete(onEndAnimation.Invoke);
        }
        else
        {
            transform.DOMove(endValue, duration, snapping).SetLoops(loopCount, type).SetEase(ease).OnComplete(onEndAnimation.Invoke);
        }
    }
    public void AnimateReverse()
    {
        if (isLocal)
        {
            transform.DOLocalMove(startPosition, duration, snapping).SetLoops(loopCount, type).SetEase(ease).OnComplete(onEndAnimationReverse.Invoke);
        }
        else
        {
            transform.DOMove(startPosition, duration, snapping).SetLoops(loopCount, type).SetEase(ease).OnComplete(onEndAnimationReverse.Invoke);
        }

    }

    public void ResetTransform()
    {
        if (isLocal)
        {
            transform.localPosition = startPosition;
        }
        else
        {
            transform.position = startPosition;
        }
    }
}
