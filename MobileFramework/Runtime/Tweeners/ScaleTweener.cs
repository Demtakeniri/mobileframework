using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class ScaleTweener : MonoBehaviour
{
    [Header("Events")]
    [SerializeField] private UnityEvent onEndAnimation;
    [SerializeField] private UnityEvent onEndAnimationReverse;

    [Header("Base Variables")]
    [SerializeField] private Transform objectToScale;
    [SerializeField] private float endScaleValue;
    [SerializeField] private float duration;
    [SerializeField] private float startScaleValue;
    [Header("Loop")]
    [SerializeField] private int loopCount;
    [SerializeField] private LoopType type;

    [Header("Ease")]
    [SerializeField] private Ease ease;



    public void Animate()
    {
        objectToScale.DOScale(endScaleValue, duration).SetEase(ease).SetLoops(loopCount, type).OnComplete(onEndAnimation.Invoke);
    }

    public void AnimateReverse()
    {
        objectToScale.DOScale(startScaleValue, duration).SetEase(ease).SetLoops(loopCount, type).OnComplete(onEndAnimationReverse.Invoke);
    }
}
