using UnityEngine;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    /// <summary>
    /// Saves the info contained inside the SaveClass variable given as input. creating or overwriting a file called saveFileName
    /// </summary>
    /// <param name="save"> SaveClass variable containing the info to save</param>
    /// <param name="saveFileName"> name of the save file</param>
    public static void Save(SaveClass save, string saveFileName)
    {
        string path = Application.persistentDataPath + saveFileName;
        BinaryFormatter formatter = GetFormatter();
        FileStream stream = new FileStream(path, FileMode.Create);
        formatter.Serialize(stream, save);
        Debug.Log("Saving...");
        stream.Close();
    }

    /// <summary>
    /// Loads and returns a SaveClass variable containing the informations saved in a file named saveFileName
    /// </summary>
    /// <param name="saveFileName"> name of the file to Load </param>
    /// <returns></returns>
    public static SaveClass Load(string saveFileName)
    {
        string path = Application.persistentDataPath + saveFileName;
        BinaryFormatter formatter = GetFormatter();
        FileStream stream = new FileStream(path, FileMode.OpenOrCreate);
        if (stream.Length>0)
        {
            Debug.Log("Loading...");
            SaveClass save = formatter.Deserialize(stream) as SaveClass;
            stream.Close();
            return save;
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Gets a special binary formatter privided with two surrogates for Vector3 and Quaternion serializations
    /// </summary>
    /// <returns> the formatter returned</returns>
    public static BinaryFormatter GetFormatter()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        SurrogateSelector selector = new SurrogateSelector();

        Vector3Surrogate v3Sur = new Vector3Surrogate();
        QuaternionSurrogate quatSur = new QuaternionSurrogate();

        selector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), v3Sur);
        selector.AddSurrogate(typeof(Quaternion), new StreamingContext(StreamingContextStates.All), quatSur);

        formatter.SurrogateSelector = selector;
        return formatter;
    }
}
