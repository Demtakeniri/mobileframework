using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    [SerializeField] AudioSource musicSource;
    [SerializeField] List<AudioSource> sfxSource;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region base functions
    /// <summary>
    /// Takes an AudioClip as input and plays it one time.
    /// </summary>
    /// <param name="clip"> the clip to play</param>
    public void PlaySfx(AudioClip clip)
    {
        bool soundPlayed = false;
        foreach (AudioSource source in sfxSource)
        {
            if (!source.isPlaying)
            {
                source.PlayOneShot(clip);
                soundPlayed = true;
                break;
            }
        }
        if (!soundPlayed)
        {
            AudioSource newSource = ObjectPooler.Instance.SpawnFromPool("AudioSource", Vector3.zero, Quaternion.identity).GetComponent<AudioSource>();
            sfxSource.Add(newSource);
            newSource.PlayOneShot(clip);
        }
    }
    /// <summary>
    /// Takes an AudioClip (music/soundtrack) as input, stops the player if is already playing a clip, change the clip to play
    /// to the given one, and plays it on loop
    /// </summary>
    /// <param name="musicClip"> the music AudioClip to play</param>
    public void PlayMusic(AudioClip musicClip)
    {
        if (musicSource.isPlaying)
        {
            musicSource.Stop();
        }
        musicSource.clip = musicClip;
        musicSource.Play();
    }
    #endregion
}
