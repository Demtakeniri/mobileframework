using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// GameManager class, used to manage anything related to the Flow of the Game, make sure to check
/// the documentaions for further details
/// <param name="saveFileName"> list of saving file names</param>
///  <param name="save"> SaveClass type variable storing data Loaded.</param>
/// </summary>
public class GameManager : Singleton<GameManager>
{
    #region variables
    private List<string> saveFileNames;
    [HideInInspector] public string currentSaveFileName { get; private set; } = "save.txt";
    public SaveClass save { get; private set; }
    #endregion

    #region Mono
    protected override void Awake()
    {
        base.Awake();
        save = SaveSystem.Load(currentSaveFileName);
    }
    #endregion

    #region SaveSystem-related functions
    public void AddNewSaveFile(string fileName)
    {
        int index = saveFileNames.Count;
        saveFileNames.Add(fileName + index.ToString() + ".txt");
    }

    public void ChangeCurrentSaveFile(int index)
    {
        if (index >= saveFileNames.Count)
        {
            Debug.LogError("index out of bound, save file does not exist!");
        }
        else
        {
            currentSaveFileName = saveFileNames[index];
        }
    }
    /// <summary>
    /// Saves the game with the current SaveClass save variable info.
    /// </summary>
    public void SaveGame()
    {
        SaveSystem.Save(save, currentSaveFileName);
    }
    #endregion

}
