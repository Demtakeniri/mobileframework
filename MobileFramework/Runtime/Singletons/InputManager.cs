using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Singleton<InputManager>
{
    [SerializeField] public float maxTimerForUnderstand { private set; get; } = .2f;
    [SerializeField] public float currentTimer { private set; get; } = 0.0f;
    [SerializeField] public float timerForDoubleTap { private set; get; } = 0f;

    private int tapCount = 0;

    private bool canDoubleTap = false;
    private bool canDetectTouch = true;
    private bool canResetTouchCount = false;

    protected override void Awake()
    {
        base.Awake();
        Input.simulateMouseWithTouches = true;
    }


    private void Update()
    {
        if (this.canDoubleTap)
        {
            this.timerForDoubleTap += Time.deltaTime;

            if (this.timerForDoubleTap > this.maxTimerForUnderstand)
            {
                this.canDoubleTap = false;
                this.tapCount = 0;
                this.timerForDoubleTap = 0;
            }
        }

        if (Input.touchCount == 0 && this.canResetTouchCount)
        {
            this.canDetectTouch = true;
            this.canResetTouchCount = false;
        }

        if (Input.touchCount == 1 && this.canDetectTouch)
        {
            GetTouchType();
        }

        if (Input.touchCount == 2)
        {
            this.canDetectTouch = false;
            this.canResetTouchCount = true;
        }
    }


    private void GetTouchType()
    {
        bool IsMoving = false;
        bool canSwipe = true;

        switch (Input.GetTouch(0).phase)
        {
            #region Began

            case TouchPhase.Began:
                print("Began");
                break;

            #endregion


            #region Moved

            case TouchPhase.Moved:
                print("Moved");
                IsMoving = true;

                if (canSwipe)
                {
                    Swipe();
                    canSwipe = false;
                }

                break;

            #endregion


            #region Stationary

            case TouchPhase.Stationary:
                print("Stationary");
                if (!IsMoving && this.currentTimer > this.maxTimerForUnderstand)
                {
                    Hold();
                }

                break;

            #endregion


            #region Ended

            case TouchPhase.Ended:
                print("Ended");
                if (this.currentTimer < this.maxTimerForUnderstand && !IsMoving)
                {
                    this.tapCount++;

                    if (this.tapCount > 1)
                        doubleTap();
                    else
                        Tap();
                }

                this.canResetTouchCount = true;
                this.currentTimer = 0.0f;

                break;

            #endregion


            #region Canceled

            case TouchPhase.Canceled:

                this.canDetectTouch = false;
                break;

            #endregion
        }

        this.currentTimer += Time.deltaTime;
    }


    #region Gesture methods
    private void Hold()
    {

    }

    private void Tap()
    {
        canDoubleTap = true;
    }

    private void doubleTap()
    {
        tapCount = 0;
    }

    private void Swipe()
    {

    }
    #endregion
}