using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ObjectPooler : Singleton<ObjectPooler>
{
    [SerializeField] Transform parent;
    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> poolDictionary;
    private Pool currentPool;

    protected override void Awake()
    {
        base.Awake();
        poolDictionary = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab, parent);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }
            poolDictionary.Add(pool.tag, objectPool);
        }
    }

    /// <summary>
    /// Takes an instance of an object from the pool with a given Tag, it activates it and spawns it at the given
    /// position and rotation
    /// </summary>
    /// <param name="tag">the string Id of the object you want to spawn</param>
    /// <param name="position"> The position where to spawn the object</param>
    /// <param name="rotation"> The rotation to give to the object spawned</param>
    /// <returns></returns>
    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
    {
        if (!poolDictionary.ContainsKey(tag))
        {
            return null;
        }
        GameObject objectToSpawn = poolDictionary[tag].Dequeue();
        if (objectToSpawn.activeInHierarchy)
        {
            foreach (Pool pool in pools)
            {
                if (pool.tag == tag)
                {
                    currentPool = pool;
                    break;
                }
            }
            if (currentPool.shouldIncreasePool)
            {
                GameObject obj = Instantiate(currentPool.prefab);
                poolDictionary[currentPool.tag].Enqueue(obj);
            }
        }

        objectToSpawn.SetActive(true);
        objectToSpawn.transform.parent = null;
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;

        poolDictionary[tag].Enqueue(objectToSpawn);
        return objectToSpawn;
    }

    /// <summary>
    /// Takes an object reference as an input and sends it back to the Pool it came from.
    /// </summary>
    /// <param name="obj"> reference of the object to send back to the Pool</param>
    public void ReturnToPool(GameObject obj)
    {
        obj.transform.position = parent.position;
        obj.transform.parent = parent;
    }
}
[System.Serializable]
public class Pool
{
    public string tag;
    public GameObject prefab;
    public int size;
    public bool shouldIncreasePool;
}
