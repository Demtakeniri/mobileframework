using UnityEngine;

public class CanvasSafeArea : MonoBehaviour
{
    [SerializeField] private Canvas canvas;
    [SerializeField] private RectTransform blackBandUp;
    [SerializeField] private RectTransform blackBandDown;
    private RectTransform rectTransform;
    private Vector2 minAnchor;
    private Vector2 maxAnchor;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        AdaptUIToScreenArea();
    }

    public void AdaptUIToScreenArea()
    {
        UpdateSafeArea();
        UpdateBlackBand();
    }

    private void UpdateSafeArea()
    {
        Rect safeArea = Screen.safeArea;

        minAnchor = safeArea.position;
        maxAnchor = minAnchor + safeArea.size;

        float width = canvas.pixelRect.width;
        float height = canvas.pixelRect.height;

        minAnchor.x /= width;
        minAnchor.y /= height;
        maxAnchor.x /= width;
        maxAnchor.y /= height;

        rectTransform.anchorMin = minAnchor;
        rectTransform.anchorMax = maxAnchor;
    }

    private void UpdateBlackBand()
    {
        blackBandUp.anchorMin = new Vector2(0, maxAnchor.y);
        blackBandDown.anchorMax = new Vector2(1, minAnchor.y);

        blackBandUp.gameObject.SetActive(true);
        blackBandDown.gameObject.SetActive(true);
    }
}
